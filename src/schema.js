// graphql-tools combines a schema string with resolvers.
import { makeExecutableSchema } from 'graphql-tools';
import fetch from 'node-fetch';

var MongoClient = require('mongodb').MongoClient;
var mongojs = require('mongojs');
var uri = "mongodb://solnreact:Skava123@cluster0-ravin-shard-00-00-1dvoa.mongodb.net:27017,cluster0-ravin-shard-00-01-1dvoa.mongodb.net:27017,cluster0-ravin-shard-00-02-1dvoa.mongodb.net:27017/skava?ssl=true&replicaSet=Cluster0-ravin-shard-0&authSource=admin";
var mongoDb = mongojs('solnreact:Skava123@cluster0-ravin-shard-00-00-1dvoa.mongodb.net:27017,cluster0-ravin-shard-00-01-1dvoa.mongodb.net:27017,cluster0-ravin-shard-00-02-1dvoa.mongodb.net:27017/skava?ssl=true&replicaSet=Cluster0-ravin-shard-0&authSource=admin', ['reactdemocart']);

//const API = 'https://adidasindiastg.skavaone.com/skavastream/core/v5/skavastore/search?campaignId=361&offset=<offset>&limit=<limit>&disableFacetMinCount=true&search=<searchTerm>&sort=&selectedFacets=&locale=en_US&storeid=adidasindia'
const plpAPI = 'https://demo.skavaone.com/skavastream/core/v5/skavastore/search?campaignId=2495&offset=<offset>&limit=<limit>&disableFacetMinCount=true&search=<searchTerm>&sort=&selectedFacets='
//Ex: https://demo.skavaone.com/skavastream/core/v5/skavastore/search?campaignId=2495&offset=0&limit=30&disableFacetMinCount=true&search=toy&sort=&selectedFacets=
const pdpAPI = 'http://demo.skavaone.com/skavastream/core/v5/skavastore/product/<productId>?campaignId=2495'
//Ex: http://demo.skavaone.com/skavastream/core/v5/skavastore/product/TOYB1?campaignId=2495

// Construct a schema, using GraphQL schema language
const typeDefs = `
  type Query {
    search(searchTerm : String, limit : Int, offset : Int) : searchProps,
    product(productId : String) : productDetail,
    getCart(cartId : String) : cartData,
    getCarts : [cartData]
  }

  type Mutation {
    addToCart(cartId: String, productId: String, image: String, pdtName: String, price: String, Qty: String): cartData,
    removeFromCart(cartId: String, productId:String) : cartData,
    removeCart(cartId: String) : [cartData]
  }

	type searchProps {
    productCount: Int
    products: [Product]
  }

  type productDetail {
    children: Product
    properties: productProps
  }
  
  type productProps {
    description: String
    currencycode: String
    price: String
  }

  type Product {
    image: String
    identifier: String
    name: String
  }

  type cartItem {
    productId: String
    image: String
    name: String
    price: String
    Qty: String
  }

  type cartData {
    cartId: String
    cartItems: [cartItem]
  }

  input addToBagData {
    cartId: Int
    productId: String
    image: String
    name: String
    price: String
    Qty: String
  }
`;

const mapJsonResp = (resp, page) => {
  let respObj = {};
  let products = [];
  if(page == 'search') {
    if(resp && resp.children && resp.children.products)
    {   
      resp.children.products.map(({name, image, identifier}) => {
        products.push({name,image,identifier})
      });
    }
    respObj.productCount = products.length
    respObj.products = products
  }
  else if(page == 'product') {        
    if(resp && resp.children && resp.children.products) {   
      resp.children.products.map(({name, image, identifier}) => {
        products.push({name,image,identifier})
      });
      respObj.children = products[0];
    }
    else {
      respObj.children = {name:resp.name, image:resp.image, identifier:resp.identifier};
    }
    respObj.properties = {
      description: resp.properties && resp.properties.iteminfo && resp.properties.iteminfo.description && resp.properties.iteminfo.description[0].value || ' ',
      currencycode: resp.properties && resp.properties.buyinfo && resp.properties.buyinfo.pricing.currencycode || '$',
      price: resp.properties && resp.properties.buyinfo && resp.properties.buyinfo.pricing.prices && resp.properties.buyinfo.pricing.prices[0].value || '50.00'
    }
  }
  return respObj
}

const removeProductFromCart = (iteminfo) => {
  return new Promise((resolve, reject) => {
    mongoDb.reactdemocart.update(
      {cart_id: iteminfo.cartId},
      {$pull:
        { products: {productId: iteminfo.productId} }
      },
      function(err, result) {
        if(err) {
          reject(err);
        }
        else {
          resolve(result);
        }
      }
    );
  });
}

const updateExistingProductInCart = (cartData) => {
  return new Promise((resolve, reject) => {
    mongoDb.reactdemocart.update(
      {cart_id: cartData.cartId, "products.productId": cartData.productId},
      {$inc: {"products.$.Qty": parseInt(cartData.Qty)}},
      function(err, result) {
        if(err) {
          reject(err);
        }
        else {
          resolve(result);
        }
      }
    );
  });
}

const updateItemToCart = async (cartData) => {
  return new Promise((resolve, reject) => {
      mongoDb.reactdemocart.update(
        {cart_id: cartData.cartId}, 
        {
          $push: 
          {
            products: 
            {
              productId: cartData.productId, 
              Qty: parseInt(cartData.Qty),
              name: cartData.pdtName,
              price: cartData.price,
              image: cartData.image
            }
          }
        },
        function(err, result) {
          if(err) {
            reject(err);
          }
          else {
            if(result.nModified > 0) {
              resolve(result);
            }
            else {
              const cartJson = { 
                "cart_id" : cartData.cartId,
                "products" : [
                  {
                    "productId": cartData.productId,
                    "Qty": parseInt(cartData.Qty),
                    "name": cartData.pdtName,
                    "price": cartData.price,
                    "image": cartData.image
                  }
                ]
              };
              mongoDb.reactdemocart.save(cartJson, function(err, result){
                if(err) {
                  reject(err);
                }
                else {
                  resolve(result);
                }
              });
            }
          }
        }
      );
  });
}

const deleteCartInstance = (cartId) => {
  return new Promise((resolve, reject) => {
    mongoDb.reactdemocart.remove( {cart_id: cartId}, function(err, result){
      if(err) {
        reject(err);
      }
      else {
        resolve(result);
      }
    });
  });
}

const fetchCartDetails = (cartId) => {
  return new Promise((resolve, reject) => {
    mongoDb.reactdemocart.find({"cart_id":cartId}, function(err, result){
      if(err) reject(err);
      //console.log('result: ' + JSON.stringify(result[0]));
      resolve(result[0]);
    });
  });
}

const fetchCartData = () => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(uri, function(err, client) {        
      if (err) throw err;
      const db = client.db('skava');  
      db.collection("reactdemocart").find({}).toArray(function(err, result) {
        if (err) reject(err);
        //console.log('result: ' + JSON.stringify(result));
        resolve(result);
        //client.close();  
      });
    });
  });
}

const replaceDynParams = (API,{searchTerm,limit,offset,productId}) => {
    return API.replace('<productId>',productId).replace('<searchTerm>',searchTerm).replace('<offset>',offset).replace('<limit>',limit);
}
// Provide resolver functions for your schema fields
const resolvers = {
  Query: {
    search: async (root, args) => {
      const results = await fetch(`${replaceDynParams(plpAPI,args)}`)
      const jsonRes = await results.json()
      return mapJsonResp(jsonRes,'search');
    },
    product: async (root, args) => {
      const results = await fetch(`${replaceDynParams(pdpAPI,args)}`)
      const jsonRes = await results.json()
      return mapJsonResp(jsonRes,'product');
    },
    getCarts: async () => {
      var respObj = [];
      await fetchCartData().then(response => {
        //console.log('response: ' + JSON.stringify(response));
        response.map(cartItem => {
          respObj.push({cartId: cartItem.cart_id, cartItems: cartItem.products});
        })
        //console.log('respObj: ' + JSON.stringify(respObj));
      }).catch(err => {console.log(err)});
      return respObj;
    },
    getCart: async (root, args) => {
      var respObj = {};
      await fetchCartDetails(args.cartId).then(response => {
        //console.log('response: ' + JSON.stringify(response));
        respObj.cartId = response.cart_id;
        respObj.cartItems = response.products;        
        //console.log('respObj: ' + JSON.stringify(respObj));
      }).catch(err => {console.log(err)});
      return respObj;
    }
  },
  Mutation: {
    addToCart: async (root, args) => {
      // console.log(args);
      let respObj = {};

      let modifyExistingProduct = false;

      await updateExistingProductInCart(args).then(result => {
        if(result.nModified > 0) {
          modifyExistingProduct = true;
        }
      }).catch(err => {console.log(err)});

      if(!modifyExistingProduct) {
        await updateItemToCart(args).then(response => {
          //console.log(response);
        }).catch(err => {console.log(err)});
      }
      
      await fetchCartDetails(args.cartId).then(response => {
        //console.log('response: ' + JSON.stringify(response));
        respObj.cartId = response.cart_id;
        respObj.cartItems = response.products;        
        //console.log('respObj: ' + JSON.stringify(respObj));
      }).catch(err => {console.log(err)});
      
      return respObj;
    },
    removeFromCart: async (root, args) => {
      let respObj = {};
      
      await removeProductFromCart(args).then(response => {
        console.log(response);
      });
      
      await fetchCartDetails(args.cartId).then(response => {
        //console.log('response: ' + JSON.stringify(response));
        respObj.cartId = response.cart_id;
        respObj.cartItems = response.products;        
        //console.log('respObj: ' + JSON.stringify(respObj));
      }).catch(err => {console.log(err)});
      
      return respObj;
    },
    removeCart: async (root, args) => {
      let respObj = [];
      let isCartDeleted = false;

      await deleteCartInstance(args.cartId).then(result => {
        //console.log('Delete Count: ' + result.deletedCount);
        if(result.deletedCount) {
          isCartDeleted = true;
        }
      }).catch(err => console.log(err));

      await fetchCartData().then(response => {
        response.map(cartItem => {
          respObj.push({cartId: cartItem.cart_id, cartItems: cartItem.products});
        })
      }).catch(err => {console.log(err)});
      
      return respObj || [{"cartId": "No Cart ID Returned"}]; 
    }
  }
};

// Required: Export the GraphQL.js schema object as "schema"
export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

// Optional: Export a function to get context from the request. It accepts two
// parameters - headers (lowercased http headers) and secrets (secrets defined
// in secrets section). It must return an object (or a promise resolving to it).
export function context(headers, secrets) {
  return {
    headers,
    secrets,
  };
};
