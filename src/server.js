import express from 'express';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import bodyParser from 'body-parser';
import opn from 'opn';
import cors from "cors";

import * as Schema from './schema';

const PORT = process.env.PORT || 4000;
const server = express();

if (typeof process.env.APOLLO_ENGINE_KEY === 'undefined') {
  process.env.APOLLO_ENGINE_KEY = "tamils360:ihhtu8Ehy2XXedw0Kx5gmA"
}

const schemaFunction =
  Schema.schemaFunction ||
  function() {
    return Schema.schema;
  };
let schema;
server.use('/graphql', cors(), bodyParser.json(), graphqlExpress(async (request,response) => {
  //response.header("Access-Control-Allow-Origin", "*");
  if (!schema) {
    schema = schemaFunction(process.env)
  }
  return {
    schema: await schema,
  };
}));

server.use('/graphiql', cors(), bodyParser.json(), graphiqlExpress({
  endpointURL: '/graphql',
  query: `# Welcome to GraphiQL
  {
    search(searchTerm:"men",limit:100,offset:0) {
        products {
          image
          identifier
          name
        }
        productCount
      }
  }
`,
}));

server.listen(PORT, () => {
  console.log(`GraphQL Server is now running on http://localhost:${PORT}/graphql`);
  console.log(`View GraphiQL at http://localhost:${PORT}/graphiql`);
  let browser = (process.platform === "win32") ? "chrome" : "google chrome";
  if(!process.env.PORT)
    opn(`http://localhost:${PORT}/graphiql`, { app: [browser] })
});